dwm - dynamic window manager
============================
dwm is an extremely fast, small, and dynamic window manager for X.


Requirements
------------
In order to build dwm you need the Xlib header files.


Patches
-------
This fork of dwm includes the following patches:

    - Actualfullscreen: actually toggles fullscreen.
    - Attachbottom: new clients attach at the bottom of the stack.
    - Awesomebar (custom): awesome taskbar but without window hiding.
    - Bar-height: change dwm's default bar height.
    - Centername_fancybar (custom): center the WM_NAME of the windows on the bar.
    - Colortags (custom): use the same scheme for occupied and selected tags.
    - Fixborders: make window borders opaque even if using a compositor.
    - Gofullscreen (custom): allows for clients to start in fullscreen mode right away.
    - Not Center (custom): specify through a rule the clients that won't be centered.
    - Push (no master): move clients around in the stacking area.
    - Removeborder: remove the border when there is one window visible.
    - Savefloats: save and restore the size and position of every floating window.
    - Setstatus: enables setting the status with dwm itself.
    - Statuscmd (nosignal): execute shell commands on clicking the bar.
    - Steam: fixes some of steam's odd behavior.
    - Togglelayout: toggle to the previous layout with the same shortcut.
    - Underlinetags: underline selected tags.
    - Unmanaged: specify windows that won't be managed by the window manager.
    - Vanitygaps (custom): adds toggle-able gaps between windows.
    - Vanity Cfacts (custom): allows resizing clients, and works on top of vanitygaps.

Most of these patches are in the suckless page, under the patches section.
The patches with the custom label are written or modified from other patches by me.
Togglelayout, Steam and Unmanaged come from bakkeby's dwm-flexipatch.
Finally, Colortags has been extracted from https://github.com/fitrh/dwm


Custom Keyboard Shortcuts
-------------------------
Super + d               spawn dmenu_run.
Super + Return          open the terminal.
Super + h/l             change the window focus, just like with j/k.
Super + I               decrease number of windows in the master area.
Super + Ctrl + h/l      change the master area size.
Super + =/-/+           change the size of the stacked clients.
Super + Alt + 0         toggle gaps on/off.
Super + J/K             move clients around in the stack area.
Super + Shift + Return  move the selected client to the master area.
Super + q               close the focused window.
Super + F               set the floating layout.
Super + s               toggle floating state on the selected client.
Super + f               toggle fullscreen for the focused window.
Super + [ / ]           focus other monitors.
Super + Shift + [ / ]   send clients to other monitors.

Rest are default.


Installation
------------
Edit config.mk to match your local setup (dwm is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install dwm (if
necessary as root):

    make clean install


Running dwm
-----------
Add the following line to your .xinitrc to start dwm using startx:

    exec dwm

In order to connect dwm to a specific display, make sure that
the DISPLAY environment variable is set correctly, e.g.:

    DISPLAY=foo.bar:1 exec dwm

(This will start dwm on display :1 of the host foo.bar.)

In order to display status info in the bar, you can do something
like this in your .xinitrc:

    while xsetroot -name "`date` `uptime | sed 's/.*,//'`"
    do
    	sleep 1
    done &
    exec dwm


Configuration
-------------
The configuration of dwm is done by creating a custom config.h
and (re)compiling the source code.
